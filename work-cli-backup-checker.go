package main

import (
	"flag"
	"fmt"
	"gitlab.com/Spouk/gotool/config"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
)

const (
	info      = "usage: %s\n"
	logprefix = "[backup-database-checker] "
	layout    = "2006-01-02"
)

//профиль файла бэкапа
type profileFile struct {
	prefix   string
	name     string
	datemake string
	datetime time.Time
	sizer    int64
	typeS    string
}

//профиль
type Profile struct {
	IP     string //ip базы в строковом представлении для формирования логов и отчетности
	Period int    //период обновления бэкапа
	Prefix string //префикс начала файла
}

//конфиг
type Config struct {
	CheckPeriod  int       `yaml:"checkperiod"`
	WorkDir      string    `yaml:"workdir"`
	LogFile      string    `yaml:"logfile"`
	LogPrefix    string    `yaml:"logprefix"`
	LogTagSyslog string    `yaml:"logtagsyslog"`
	ReportFile   string    `yaml:"reportfile"`
	Databases    []Profile `yaml:"databases"`
	Production   bool      `yaml:"production"`
	Debug        bool      `yaml:"debug"`
}

//основный инстанс
type BackupChecker struct {
	sw            *sync.WaitGroup
	Log           *log.Logger
	c             *Config
	logfile       *os.File
	StockFiles    map[string]profileFile //1 файл по одному префиксу
	StockTMPFIles []profileFile          //общий сток всех файлов выбранных по регспекам
	listProfiles  map[string]Profile
	linuxRegex    *regexp.Regexp
	winRegex      *regexp.Regexp
}

//создание нового инстанса
func NewBackupChecker(configFile string) *BackupChecker {
	bc := &BackupChecker{
		sw:           new(sync.WaitGroup),
		Log:          log.New(os.Stdout, logprefix, log.LstdFlags),
		c:            new(Config),
		StockFiles:   make(map[string]profileFile),
		listProfiles: make(map[string]Profile),
	}
	//чтение конфига
	conf := config.NewConf("/tmp", os.Stdout)
	err := conf.ReadConfig(configFile, bc.c)
	if err != nil {
		panic(err)
	}
	//чтение/создагние лога
	if _, err := os.Stat(bc.c.LogFile); os.IsNotExist(err) {
		fh, errf := os.OpenFile(bc.c.LogFile, os.O_CREATE|os.O_RDWR, os.ModePerm)
		if errf != nil {
			panic(errf)
		}
		bc.logfile = fh
	} else {
		fh, errf := os.OpenFile(bc.c.LogFile, os.O_APPEND|os.O_RDWR, os.ModePerm)
		if errf != nil {
			panic(errf)
		}
		bc.logfile = fh
	}
	//компиляция регспеков
	bc.linuxRegex = regexp.MustCompile(`t\d\d\d-\w\w\d-\d\d\d\d\d\d\d\d.7z`)
	bc.winRegex = regexp.MustCompile(`t\d\d\d-\w\w\d-\d\d\d\d\d\d\d\d[wW].7z`)

	//иницализация карты под профили
	bc.listProfiles = make(map[string]Profile)
	for _, x := range bc.c.Databases {
		bc.listProfiles[x.Prefix] = x
	}
	return bc
}

//враппер для записи в лог
func (bc *BackupChecker) writeLog(msg string) {
	_, _ = bc.logfile.WriteString(
		fmt.Sprintf("%s [%v] %s\n", logprefix, time.Now().Format("2006-01-02 15:04"), msg))
	bc.Log.Print(msg)
}

//формирует список для обработки
//шаблон t000-20200902.7z
//regexp.MustCompile(`t([0-9])([0-9])([0-9])-(.*).7z`)
func (bc *BackupChecker) parseWorkDir() {
	err := filepath.Walk(bc.c.WorkDir, bc.checker)
	if err != nil {
		panic(err)
	}
}

//функция отбора по регспекам подпадающих под шаблон файлов
func (bc *BackupChecker) checker(path string, info os.FileInfo, err error) error {
	if info.IsDir() == false {
		//определение локальных переменных
		var (
			pp       = profileFile{}
			datetime time.Time
		)

		//конвертация временной метки
		if bc.winRegex.MatchString(info.Name()) || bc.linuxRegex.MatchString(info.Name()) {
			mark := info.Name()[9:17]
			conv := strings.Join([]string{mark[:4], mark[4:6], mark[6:]}, "-")
			tt, err := time.Parse(layout, conv)
			if err != nil {
				log.Println(err)
			} else {
				datetime = tt
			}
			//window backup
			if bc.winRegex.MatchString(info.Name()) {
				pp = profileFile{
					prefix:   info.Name()[:4],
					name:     info.Name(),
					datemake: info.Name()[9:17],
					datetime: datetime,
					sizer:    info.Size(),
					typeS:    "windows",
				}
			}
			//linux backup
			if bc.linuxRegex.MatchString(info.Name()) {
				pp = profileFile{
					prefix:   info.Name()[:4],
					name:     info.Name(),
					datemake: info.Name()[9:17],
					datetime: datetime,
					sizer:    info.Size(),
					typeS:    "linux",
				}
			}
			//сохраняю результат
			bc.StockTMPFIles = append(bc.StockTMPFIles, pp)
		}
	}
	return nil
}

//формирования конечного списка, (n^2) todo: оптимизировать позже
func (w *BackupChecker) MakeEndListFiles() {
	for _, x := range w.StockTMPFIles {
		w.StockFiles[x.prefix] = x
		for _, a := range w.StockTMPFIles {
			if a.prefix == x.prefix && a.datetime.Unix() > x.datetime.Unix() {
				if w.c.Debug {
					log.Printf("[%v][%v][%v] %v %v\n", a.name, a.datetime.Unix() > x.datetime.Unix(), x.name, a.datetime.Unix(), x.datetime.Unix())
				}
				w.StockFiles[x.prefix] = a
			}
		}
	}
}

//одиночная проверка
func (bc *BackupChecker) RunSingleChecker() {
	//парсю входную директорию
	bc.parseWorkDir()
	//формирую конечный список файлов
	bc.MakeEndListFiles()
	//проверяю файлы на наличие просроченности бэкапов
	bc.checkBackupFileWithProfile()
	for key, x := range bc.StockFiles {
		bc.Log.Printf("%v %v\n", key, x)
	}
	bc.Log.Printf("одиночная проверка успешно завершена\n")
}

//проверка на протухшие бэкапы
func (bc *BackupChecker) checkBackupFileWithProfile() {
	//создаю файл для отчета
	reportFile, e := os.OpenFile(bc.c.ReportFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.ModePerm)
	if e != nil {
		bc.writeLog(e.Error())
		panic(e)
	}
	//проверяю файлы на наличие протухлости
	for k, val := range bc.listProfiles {
		if v, found := bc.StockFiles[k]; found {
			//профиль по данной базе найден, проверка по времени создания и периода обновления бэкапа
			if bc.checkTimeFile(v, val.Period) == false {
				//ошибка, период обновления бэкапа истек а нового файла нет, пишу в лог + файл репорт проверки
				_, err := reportFile.WriteString(fmt.Sprintf("%s [%v] %v - %v\n", logprefix, time.Now().String(), val.IP, "бэкап протух"))
				if err != nil {
					bc.writeLog(err.Error())
				}
				bc.writeLog(fmt.Sprintf("[%s] %s  %v", val.Prefix, val.IP, "бэкап протух"))
			} else {
				bc.writeLog(fmt.Sprintf("[%s] %s  %v", val.Prefix, val.IP, "ОК"))
			}
		} else {
			//не найден файл по профилю - ошибка
			_, err := reportFile.WriteString(fmt.Sprintf("%s [%v] %v - %v\n", logprefix, time.Now().String(), val.IP, "бэкапа НЕТ"))
			if err != nil {
				bc.writeLog(err.Error())
			}
			bc.writeLog(fmt.Sprintf("%s [%v] %v - %v", logprefix, time.Now().String(), val.IP, "бэкапа НЕТ"))
		}
	}
}

//проверка по таймеру проверка
func (bc *BackupChecker) RunTimerChecker() {
	bc.sw.Add(1)
	go bc.Checker()
	bc.sw.Wait()
}

func (bc *BackupChecker) ShowListFiles() {
	for _, x := range bc.StockFiles {
		bc.Log.Printf("%v", x.name)
	}
}

//горутина по проверке файлов, пока линейная проверка для тестирования работы алгоритма
//сложность n^2 - печально конечно ))
func (bc *BackupChecker) Checker() {
	bc.writeLog("checker запущен")
	defer func() {
		bc.sw.Done()
		bc.writeLog("горутина checker закончила работу")
	}()
	ticker := time.NewTicker(time.Second * time.Duration(bc.c.CheckPeriod))
	for {
		select {
		case t := <-ticker.C:
			bc.writeLog(fmt.Sprintf("новая итерация-проверки - %v", t.String()))

			//формирую свеженький список файлов
			bc.StockTMPFIles = []profileFile{}
			bc.StockFiles = make(map[string]profileFile)
			//парсю входную директорию
			bc.parseWorkDir()
			//формирую конечный список файлов
			bc.MakeEndListFiles()

			//создаю файл для отчета
			reportFile, e := os.OpenFile(bc.c.ReportFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.ModePerm)
			if e != nil {
				bc.writeLog(e.Error())
			}
			//проверяю файлы на наличие протухлости
			for k, val := range bc.listProfiles {
				if v, found := bc.StockFiles[k]; found {
					//профиль по данной базе найден, проверка по времени создания и периода обновления бэкапа
					if bc.checkTimeFile(v, val.Period) == false {
						//ошибка, период обновления бэкапа истек а нового файла нет, пишу в лог + файл репорт проверки
						_, err := reportFile.WriteString(fmt.Sprintf("%s [%v] %v - %v\n", logprefix, time.Now().String(), val.IP, "бэкап протух"))
						if err != nil {
							bc.writeLog(err.Error())
						}
						bc.writeLog(fmt.Sprintf("[%s] %s  %v", val.Prefix, val.IP, "бэкап протух"))
					} else {
						bc.writeLog(fmt.Sprintf("[%s] %s  %v", val.Prefix, val.IP, "ОК"))
					}
				} else {
					//не найден файл по профилю - ошибка
					_, err := reportFile.WriteString(fmt.Sprintf("%s [%v] %v - %v\n", logprefix, time.Now().String(), val.IP, "бэкапа НЕТ"))
					if err != nil {
						bc.writeLog(err.Error())
					}
					bc.writeLog(fmt.Sprintf("%s [%v] %v - %v", logprefix, time.Now().String(), val.IP, "бэкапа НЕТ"))
				}
			}
		}
	}
}

//проверяет период даты создания файла
func (bc *BackupChecker) checkTimeFile(v profileFile, period int) bool {
	fTime := v.datetime
	//получаю текущую дату
	tnow := time.Now()
	//добавляю период в часах ко времени создания файла
	fTime = fTime.Add(time.Second * time.Duration(period))
	if bc.c.Debug {
		bc.Log.Printf("[debug][%v] %v %v %v\n", v.name, tnow.Format(layout), tnow.Unix() > fTime.Unix(), fTime.Format(layout))
	}
	//сравниваю
	if tnow.Unix() > fTime.Unix() {
		//ошибка, период обновления бэкапа истек а нового файла нет, пишу в лог + файл репорт проверки
		return false
	} else {
		return true
	}
}

func main() {
	var (
		configPath string
		single     bool
		timer      bool
	)
	flag.BoolVar(&single, "single", false, "разовая проверка бэкапов")
	flag.BoolVar(&timer, "timer", false, "проверка бэкапов по таймеру")
	flag.StringVar(&configPath, "config", "", "файл конфигурации с полным путем к нему")
	flag.Parse()
	flag.Usage = func() {
		fmt.Printf(info, os.Args[0])
		flag.PrintDefaults()
	}
	//fmt.Printf("Len: %v %v %v\n", len(os.Args), single, timer)
	if len(os.Args) == 1 {
		flag.Usage()
		os.Exit(1)
	} else if len(os.Args) == 4 {

		if (single && timer) || single {
			bc := NewBackupChecker(configPath)
			bc.RunSingleChecker()
			os.Exit(1)
		}
		if (single == false) && timer {
			bc := NewBackupChecker(configPath)
			bc.RunTimerChecker()
			os.Exit(1)
		}

	} else {
		flag.Usage()
		os.Exit(1)
	}

	//bc := NewBackupChecker(configPath)
	////pp := "/home/spouk/stock/s.develop/go/src/gitlab.com/work-backup-cli-checker/config.yaml"
	//bc := NewBackupChecker(pp)
	//bc.RunSingleChecker()
}
